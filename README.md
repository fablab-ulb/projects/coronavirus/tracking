## Coronavirus tracking projects for Fablab ULB

### Active projects at Fablab ULB
- Protective Face Shields ([repo](https://gitlab.com/fablab-ulb/projects/coronavirus/protective-face-shields), [website](https://fablab-ulb.gitlab.io/projects/coronavirus/protective-face-shields/))
- Adapting firefighter masks into protective masks ([website](https://fablab-ulb.gitlab.io/projects/coronavirus/firefighter-masks-connector/))
- Nasopharyngeal Swabs ([repo](https://gitlab.com/fablab-ulb/projects/coronavirus/nasopharyngeal-swabs))
- Medical gowns
- Mask strap ([repo](https://gitlab.com/fablab-ulb/projects/coronavirus/mask-strap))
- Cloth Face Mask ([repo](https://gitlab.com/fablab-ulb/projects/coronavirus/cloth-face-masks)[website](https://fablab-ulb.gitlab.io/projects/coronavirus/cloth-face-masks/))

### Fab Lab global development and deployment
- group: https://gitlab.fabcloud.org/pub/project/coronavirus  
- project: https://gitlab.fabcloud.org/pub/project/coronavirus/tracking

###  MIT CBA Coronavirus tracking project

- MIT CBA tracking project: https://gitlab.cba.mit.edu/pub/coronavirus/tracking/
